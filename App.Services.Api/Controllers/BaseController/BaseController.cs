﻿using App.Application.Interfaces;
using App.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace App.Services.Api.Controllers
{
    public class BaseController<Entity> : Controller
        where Entity : BaseEntity
    {
        readonly protected IAppBase<Entity> app;

        public BaseController(IAppBase<Entity> app)
        {
            this.app = app;
        }
    }
}

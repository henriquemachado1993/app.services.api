﻿using App.Application.Interfaces;
using App.Domain.Entities;

namespace App.Services.Api.Controllers
{
    public class Example: BaseController<Prato>
    {
        public Example(IPratoApp app)
            : base(app)
        { }
    }
}

﻿using App.Application.Interfaces;
using App.Application.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Arc.Common.Entities;
using Arc.Common.Utilities;

namespace App.Services.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/prato")]
    public class PratoController : Controller
    {
        readonly protected IPratoApp _pratoApp;
        public PratoController(IPratoApp app)
        {
            this._pratoApp = app;
        }

        [HttpGet]
        [Route("")]
        public TransactionInformation<List<PratoRp>> Listar([FromBody] PratoRq rq)
        {
            try
            {
                var lstPrato = _pratoApp.Listar(rq);
                return BaseApiController.ReturnModel<List<PratoRp>>(lstPrato);
            }
            catch (Exception ex)
            {
                return BaseApiController.ReturnError<List<PratoRp>>(ex);
            }
        }

        [HttpGet]
        [Route("{id}")]
        public TransactionInformation<PratoRp> PegarPorId(int id)
        {
            try
            {
                var restaurantes = _pratoApp.PegarPorId(id);
                return BaseApiController.ReturnModel<PratoRp>(restaurantes);
            }
            catch (Exception ex)
            {
                return BaseApiController.ReturnError<PratoRp>(ex);
            }
        }

        [HttpPost]
        public TransactionInformation<PratoRp> Incluir([FromBody] PratoRq rq)
        {
            try
            {
                var prato = _pratoApp.Incluir(rq);
                return BaseApiController.ReturnModel<PratoRp>(prato);
            }
            catch (Exception ex)
            {
                return BaseApiController.ReturnError<PratoRp>(ex);
            }
        }

        [HttpPut]
        public TransactionInformation<PratoRp> Alterar([FromBody] PratoRq rq)
        {
            try
            {
                var _prato = _pratoApp.Alterar(rq);
                return BaseApiController.ReturnModel<PratoRp>(_prato);
            }
            catch (Exception ex)
            {
                return BaseApiController.ReturnError<PratoRp>(ex);
            }
        }

        [HttpDelete]
        [Route("{id}")]
        public TransactionInformation<bool> Excluir(int id)
        {
            try
            {
                _pratoApp.Excluir(id);
                return BaseApiController.ReturnModel<bool>(true);
            }
            catch (Exception ex)
            {
                return BaseApiController.ReturnError<bool>(ex);
            }
        }
    }
}

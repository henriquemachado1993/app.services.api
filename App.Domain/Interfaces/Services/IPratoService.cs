﻿using App.Domain.Entities;

namespace App.Domain.Interfaces.Services
{
    public interface IPratoService : IBaseService<Prato>
    {
    }
}

﻿using App.Domain.Entities;
using Arc.Common.VO;
using System.Collections.Generic;

namespace App.Domain.Interfaces.Repository
{
    public interface IBaseRepository<T>
        where T : BaseEntity
    {
        T Add(T entity);
        void Delete(int id);
        void Delete(T entity);
        T Update(T entity);
        T GetById(int id);
        IEnumerable<T> Get(QueryCriteria<T> query = null);
    }
}

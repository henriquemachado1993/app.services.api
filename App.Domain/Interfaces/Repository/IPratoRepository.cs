﻿using App.Domain.Entities;

namespace App.Domain.Interfaces.Repository
{
    public interface IPratoRepository : IBaseRepository<Prato>
    {        
    }
}

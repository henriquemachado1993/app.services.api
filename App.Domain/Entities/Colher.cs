﻿using System.Collections.Generic;

namespace App.Domain.Entities
{
    public class Colher : BaseEntity
    {
        public int Qnt { get; set; }

        public virtual ICollection<Prato> Pratos { get; set; }
    }
}

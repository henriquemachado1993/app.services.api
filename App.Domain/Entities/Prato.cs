﻿namespace App.Domain.Entities
{
    public class Prato: BaseEntity
    {
        public string Nome { get; set; }
        public double Preco { get; set; }

        public int TipoId { get; set; }        
        public int ColherId { get; set; }
        public Tipo Tipo { get; set; }
        public Colher Colher { get; set; }        
    }
}

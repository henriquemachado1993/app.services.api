﻿using System.Collections.Generic;

namespace App.Domain.Entities
{
    public class Tipo : BaseEntity    
    {
        public string Nome { get; set; }
        public int TipoPrato { get; set; }

        public virtual ICollection<Prato> Pratos { get; set; }
    }
}

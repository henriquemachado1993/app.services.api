﻿using App.Domain.Entities;
using App.Domain.Interfaces.Repository;
using App.Domain.Interfaces.Services;

namespace App.Domain.Services
{
    public class PratoService : BaseService<Prato>, IPratoService
    {
        public PratoService(IPratoRepository rep) 
            : base(rep)
        {
        }
    }
}

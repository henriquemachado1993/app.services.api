﻿using App.Domain.Entities;
using App.Domain.Interfaces.Repository;
using App.Domain.Interfaces.Services;
using Arc.Common.VO;
using System.Collections.Generic;

namespace App.Domain.Services
{
    public class BaseService<T> : IBaseService<T> where T : BaseEntity
    {
        protected readonly IBaseRepository<T> rep;

        public BaseService(IBaseRepository<T> rep)
        {
            this.rep = rep;
        }

        public T Add(T entity)
        {
            return this.rep.Add(entity);
        }

        public void Delete(int id)
        {
            this.rep.Delete(id);
        }

        public void Delete(T entity)
        {
            this.rep.Delete(entity);
        }

        public T GetById(int id)
        {
            return this.rep.GetById(id);
        }

        public IEnumerable<T> Get(QueryCriteria<T> query = null)
        {
            return this.rep.Get(query);
        }

        public T Update(T entity)
        {
            return this.rep.Update(entity);
        }
    }
}

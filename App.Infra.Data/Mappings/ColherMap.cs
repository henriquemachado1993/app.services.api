﻿using App.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace App.Infra.Data.Mappings
{
    public class ColherMap : MapBase<Colher>
    {
        public override void Configure(EntityTypeBuilder<Colher> builder)
        {
            base.Configure(builder);
            builder.ToTable("colher");
            builder.Property(c => c.Qnt).HasColumnName("Qnt");
            builder.HasMany(x => x.Pratos);
        }
    }
}

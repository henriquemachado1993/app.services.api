﻿using App.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace App.Infra.Data.Mappings
{
    public class TipoMap : MapBase<Tipo>
    {
        public override void Configure(EntityTypeBuilder<Tipo> builder)
        {
            base.Configure(builder);
            builder.ToTable("tipo");
            builder.Property(c => c.Nome).HasColumnName("Nome").HasMaxLength(100);
            builder.Property(c => c.TipoPrato).HasColumnName("TipoPrato");
            builder.HasMany(x => x.Pratos);
        }
    }
}

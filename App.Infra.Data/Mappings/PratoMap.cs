﻿using App.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace App.Infra.Data.Mappings
{
    public class PratoMap : MapBase<Prato>
    {
        public override void Configure(EntityTypeBuilder<Prato> builder)
        {
            base.Configure(builder);
            builder.ToTable("prato");
            builder.Property(c => c.Nome).HasColumnName("Nome").HasMaxLength(100);
            builder.Property(c => c.Preco).HasColumnName("Preco");
            builder.HasOne(x => x.Tipo).WithMany(b => b.Pratos);
            builder.HasOne(x => x.Colher).WithMany(b => b.Pratos);
        }
    }
}

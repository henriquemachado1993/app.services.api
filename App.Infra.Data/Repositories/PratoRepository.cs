﻿using App.Domain.Entities;
using App.Domain.Interfaces.Repository;
using App.Infra.Data.Contexts;

namespace App.Infra.Data.Repositories
{
    public class PratoRepository : BaseRepository<Prato>, IPratoRepository
    {
        public PratoRepository(Context contexto)
            : base(contexto)
        {
        }
    }
}

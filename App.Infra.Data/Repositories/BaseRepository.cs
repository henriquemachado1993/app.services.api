﻿using App.Domain.Entities;
using App.Domain.Interfaces.Repository;
using App.Infra.Data.Contexts;
using Arc.Common.VO;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace App.Infra.Data.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T>
        where T : BaseEntity
    {
        protected readonly Context context;
      
        public BaseRepository(Context context)
        {
            this.context = context;
        }

        public T Add(T entity)
        {
            T _entity = null;
            if(entity != null)
            {
                context.InitTransacao();
                _entity = context.Set<T>().Add(entity).Entity;
                context.SendChanges();
            }           
            return _entity;
        }

        public void Delete(int id)
        {
            var _entity = GetById(id);
            if (_entity != null)
            {
                context.InitTransacao();
                context.Set<T>().Remove(_entity);
                context.SendChanges();
            }            
        }

        public void Delete(T entity)
        {
            if (entity != null)
            {
                context.InitTransacao();
                context.Set<T>().Remove(entity);
                context.SendChanges();
            }
        }

        public T GetById(int id)
        {
            return context.Set<T>().Find(id);
        }

        public IEnumerable<T> Get(QueryCriteria<T> query = null)
        {
            IQueryable<T> result = null;
            if (query == null || query.Expression == null)
                result = context.Set<T>();          
            else if (query.Expression != null)
                result = context.Set<T>().Where(query.Expression);
            if (!string.IsNullOrEmpty(query.Navigation))
            {
                var lstNavigation = query.Navigation.Split(',');
                foreach (var nav in lstNavigation)
                {
                    result = result.Include(nav);
                }                
            }
            return result.ToList();
        }

        public T Update(T entity)
        {            
            if(entity != null)
            {
                context.InitTransacao();
                context.Set<T>().Attach(entity);
                context.Entry(entity).State = EntityState.Modified;
                context.SendChanges();
            }
            return entity;
        }
    }
}

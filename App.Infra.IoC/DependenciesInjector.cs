﻿using App.Application.Interfaces;
using App.Application.Services;
using App.Domain.Interfaces.Repository;
using App.Domain.Interfaces.Services;
using App.Domain.Services;
using App.Infra.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace App.Infra.IoC
{
    public class DependenciesInjector
    {
        public static void Register(IServiceCollection svcCollection)
        {
            //Application
            svcCollection.AddScoped(typeof(IAppBase<>), typeof(BaseServiceApp<>));
            svcCollection.AddScoped<IPratoApp, PratoApp>();

            //Domain
            svcCollection.AddScoped(typeof(IBaseService<>), typeof(BaseService<>));
            svcCollection.AddScoped<IPratoService, PratoService>();

            //Repository
            svcCollection.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            svcCollection.AddScoped<IPratoRepository, PratoRepository>();
        }
    }
}

﻿using App.Application.Interfaces;
using App.Domain.Entities;
using App.Domain.Interfaces.Services;
using Arc.Common.VO;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace App.Application.Services
{
    public class BaseServiceApp<T> : IAppBase<T>
        where T : BaseEntity
    {
        protected readonly IBaseService<T> service;

        public BaseServiceApp(IBaseService<T> service)
            : base()
        {
            this.service = service;
        }

        public T Add(T entity)
        {
            return this.service.Add(entity);
        }

        public void Delete(int id)
        {
            this.service.Delete(id);
        }

        public void Delete(T entity)
        {
            this.service.Delete(entity);
        }

        public T GetById(int id)
        {
            return this.service.GetById(id);
        }

        public IEnumerable<T> Get(QueryCriteria<T> query = null)
        {
            return service.Get(query);
        }

        public T Update(T entity)
        {
            return this.service.Update(entity);
        }
    }
}

﻿using System.Collections.Generic;
using App.Application.Adapter;
using App.Application.Interfaces;
using App.Application.ViewModel;
using App.Domain.Entities;
using App.Domain.Interfaces.Services;
using Arc.Common.VO;

namespace App.Application.Services
{
    public class PratoApp : BaseServiceApp<Prato>, IPratoApp
    {
        public PratoApp(IPratoService service)
            : base( service)
        {
        }

        #region Métodos públicos

        public PratoRp Incluir(PratoRq rq)
        {
            var prato = Add(PratoAdp.MontarRq(rq));
            return PratoAdp.Montar(prato);
        }

        public PratoRp Alterar(PratoRq rq)
        {
            var prato = GetById(rq.Id);
            prato.Nome = rq.Nome;
            prato.Preco = rq.Preco;
            prato.TipoId = rq.IdTipo;
            prato.ColherId = rq.IdColher;
            var _prato = Update(prato);
            return PratoAdp.Montar(_prato);
        }

        public void Excluir(int id)
        {
            Delete(id);
        }

        public List<PratoRp> Listar(PratoRq rq)
        {
            var query = new QueryCriteria<Prato>();
            if (rq != null && !string.IsNullOrEmpty(rq.Nome))
                query.Expression = x => x.Nome == rq.Nome;
            query.Navigation = "Tipo,Colher";
            var lstPrato = Get(query) as List<Prato>;
            return PratoAdp.Listar(lstPrato);
        }

        public PratoRp PegarPorId(int id)
        {
            var prato = GetById(id);
            return PratoAdp.Montar(prato);
        }

        #endregion

        #region Métodos internos e privados

        #endregion
    }
}

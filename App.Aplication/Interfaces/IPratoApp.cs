﻿using App.Application.ViewModel;
using App.Domain.Entities;
using System.Collections.Generic;

namespace App.Application.Interfaces
{
    public interface IPratoApp : IAppBase<Prato>
    {
        PratoRp Incluir(PratoRq rq);
        PratoRp Alterar(PratoRq rq);
        void Excluir(int id);
        List<PratoRp> Listar(PratoRq rq);
        PratoRp PegarPorId(int id);
    }
}

﻿using App.Application.ViewModel;
using App.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace App.Application.Adapter
{
    public static class PratoAdp
    {
        public static List<PratoRp> Listar(List<Prato> _lstPrato)
        {
            List<PratoRp> lstPrato = new List<PratoRp>();
            if (!_lstPrato.Any())
                return lstPrato;
            foreach (var item in _lstPrato)
            {
                lstPrato.Add(Montar(item));
            }
            return lstPrato;
        }

        public static PratoRp Montar(Prato _prato)
        {
            PratoRp prato = new PratoRp();
            if (_prato == null)
                return prato;
            prato.Id = _prato.Id;
            prato.Nome = _prato.Nome;
            prato.Preco = _prato.Preco;            
            if(_prato.Tipo == null)
                return prato;
            prato.Tipo = new TipoRp() {
                Id = _prato.Tipo.Id,
                Nome = _prato.Tipo.Nome,
                TipoPrato = _prato.Tipo.TipoPrato
            };
            if (_prato.Colher == null)
                return prato;
            prato.Colher = new ColherRp()
            {
                Id = _prato.Colher.Id,
                Qnt = _prato.Colher.Qnt                
            };
            return prato;
        }

        public static Prato MontarRq(PratoRq _prato)
        {
            Prato prato = new Prato();
            if (_prato == null)
                return null;
            prato.Id = _prato.Id;
            prato.Nome = _prato.Nome;
            prato.Preco = _prato.Preco;
            prato.TipoId = _prato.IdTipo;
            prato.ColherId = _prato.IdColher;
            return prato;
        }
    }
}

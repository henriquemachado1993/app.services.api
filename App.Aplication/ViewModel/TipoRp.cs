﻿using App.Domain.Entities;

namespace App.Application.ViewModel
{
    public class TipoRp : BaseEntity
    {
        public string Nome { get; set; }
        public int TipoPrato { get; set; }
    }
}

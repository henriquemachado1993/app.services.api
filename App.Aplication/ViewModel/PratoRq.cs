﻿using System;

namespace App.Application.ViewModel
{
    public class PratoRq
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public Double Preco { get; set; }
        public int IdTipo { get; set; }
        public int IdColher { get; set; }
    }
}

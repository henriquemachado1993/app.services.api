﻿using System;

namespace App.Application.ViewModel
{
    public class PratoRp
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public Double Preco { get; set; }
        public String PrecoFormatado
        {
            get
            {
                return String.Format("{0:C}", Preco);
            }
        }
        public TipoRp Tipo { get; set; }
        public ColherRp Colher { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace App.Application.Common.Enum
{
    public enum EnumTeste
    {
        [Display(Name = "tessssssste 1")]
        teste1 = 1,
        [Display(Name = "tessssssste 2")]
        teste2 = 2,
        [Display(Name = "tessssssste 3")]
        teste3 = 3
    }
}
